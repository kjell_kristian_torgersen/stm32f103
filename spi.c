#include "config.h"

#if SPI_NUM_BUSES > 0
#include "main.h"

#include "api/spi.h"

typedef struct SPI_HandleTypeDef SPI_HandleTypeDef;

struct spi {
    SPI_HandleTypeDef* handle;
};

spi_t spi;

spi_t* spi_init(SPI_HandleTypeDef* handle)
{
    spi.handle = handle;
    HAL_SPI_Init(spi.handle);
    return &spi;
}

void spi_setMode(spi_t* this, spi_mode_t mode)
{
    // TODO: implement
}

int spi_read(spi_t* this, void* buf, int count)
{
    HAL_SPI_Receive(this->handle, buf, count, 100);
    return count;
}

int spi_write(spi_t* this, const void* buf, int count)
{
    HAL_SPI_Transmit(this->handle, (void*)buf, count, 100);
    return count;
}

int spi_readwrite(spi_t* this, void* rxbuf, const void* txbuf, int count)
{
    HAL_SPI_TransmitReceive(this->handle, (void*)txbuf, rxbuf, count, 100);
    return count;
}

void spi_close(spi_t* this)
{
    HAL_SPI_DeInit(this->handle);
}
#endif
