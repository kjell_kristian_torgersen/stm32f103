#include <string.h>

#include "main.h"

#include "api/flash.h"

void mcu_enableIRQs(void)
{
    __enable_irq();
}

void mcu_disableIRQs(void)
{
    __disable_irq();
}

ret_t mcu_flash_read(uint32_t addr, void* data, uint32_t count)
{
    memcpy(data, (const void*)addr, count);
    return count;
}

ret_t mcu_flash_write(uint32_t addr, const void* data, uint32_t count)
{

    HAL_FLASH_Unlock();
    for (int i = 0; i < count / 2; i++) {

        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *(uint16_t*)(data + 2 * i)) == HAL_OK) {
            addr += 2;
        } else {
            HAL_FLASH_Lock();
            return RET_ERROR;
        }
    }
    HAL_FLASH_Lock();
    return count;
}

ret_t mcu_flash_erase(uint32_t addr, uint32_t count)
{
    FLASH_EraseInitTypeDef erase;
    uint32_t page_error;

    erase.TypeErase = FLASH_TYPEERASE_PAGES;
    erase.PageAddress = addr;
    erase.NbPages = count / FLASH_PAGE_SIZE;

    HAL_FLASH_Unlock();
    if (HAL_FLASHEx_Erase(&erase, &page_error) != HAL_OK) {

        return RET_ERROR;
    }
    HAL_FLASH_Lock();
    return RET_OK;
}

const flash_get_size_t* mcu_flash_info(void)
{
    static const flash_get_size_t size = {
        .total_size = 128 * FLASH_PAGE_SIZE,
        .page_size = FLASH_PAGE_SIZE,
    };
    return &size;
}