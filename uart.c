#include <string.h>

#include "config.h"

#if UART_NUM_BUSES > 0

#include "main.h"

#include "api/uart.h"
#include "utils/ringbuf.h"

#include "utils/misc.h"

struct uart {
    UART_HandleTypeDef* huart;
    ringbuf_t rxbuf;
    ringbuf_t txbuf;
    uint8_t rxbyte;
    uint8_t txbyte;
    uart_tx_done_t tx_done_callback;
    void* tx_done_arg;
};

static uart_t uart[UART_NUM_BUSES] = { 0 };

uart_t* find_free_uart(void)
{
    for (int i = 0; i < UART_NUM_BUSES; i++) {
        if (uart[i].huart == NULL) {
            return &uart[i];
        }
    }
    return NULL;
}

ret_t uart_set_tx_done_callback(uart_t* this, uart_tx_done_t callback, void* arg)
{
    this->tx_done_callback = callback;
    this->tx_done_arg = arg;
    return RET_OK;
}

uart_t* uart_init(UART_HandleTypeDef* huart, uint32_t baudRate, void* rxbuf, int rxbufsize, void* txbuf, int txbufsize)
{
    uart_t* this = find_free_uart();
    if (this) {
        memset(this, 0, sizeof(uart_t));
        this->huart = huart;
        ringbuf_init(&this->rxbuf, rxbuf, rxbufsize);
        ringbuf_init(&this->txbuf, txbuf, txbufsize);
        HAL_UART_Receive_IT(huart, &this->rxbyte, 1);
    }
    return this;
}

static ret_t startTransmit(uart_t* this)
{
    HAL_UART_StateTypeDef state = HAL_UART_GetState(this->huart);
    if (!((state & HAL_UART_STATE_BUSY_TX) & 3)) {
        if (ringbuf_read(&this->txbuf, &uart->txbyte, 1) == 1) {
            HAL_UART_Transmit_IT(this->huart, &uart->txbyte, 1);
        } else {
            if (this->tx_done_callback) {
                this->tx_done_callback(this->tx_done_arg);
            }
        }
        return RET_OK;
    }
    return RET_NOT_AVAILABLE;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
    for (int i = 0; i < UART_NUM_BUSES; i++) {
        if (uart[i].huart == huart) {
            ringbuf_write(&uart[i].rxbuf, &uart[i].rxbyte, 1);
            HAL_UART_Receive_IT(huart, &uart[i].rxbyte, 1);
            return;
        }
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart)
{
    for (int i = 0; i < UART_NUM_BUSES; i++) {
        if (uart[i].huart == huart) {
            startTransmit(&uart[i]);
            return;
        }
    }
}

ret_t uart_read(uart_t* this, void* data, int count)
{
    //__disable_irq();
    int ret = ringbuf_read(&this->rxbuf, data, count);
    //__enable_irq();
    return ret;
}

ret_t uart_write(uart_t* this, const void* buf, int count)
{
    // memcpy(txBuf, buf, MIN(count,sizeof(txBuf)));
    /*if(HAL_UART_Transmit(this->huart, (void*)buf, count, 10) == HAL_OK) {
            return count;
    }*/
    int n = ringbuf_write(&this->txbuf, buf, count);
    startTransmit(this);
    return n;

    return 0;
}

ret_t uart_close(uart_t* this)
{
    this->huart = NULL;
    return RET_OK;
}

/*void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

        ringbuf_write(&uart.rxbuf, &rxbyte, 1);
        HAL_UART_Receive_IT(huart, &rxbyte, 1);
}

uart_t* uart_init(UART_HandleTypeDef *huart, uint32_t baudRate, void *rxbuf, int rxbufsize, void *txbuf, int txbufsize)
{
        uart.huart = huart;
        ringbuf_init(&uart.rxbuf, rxbuf, rxbufsize);
        ringbuf_init(&uart.txbuf, txbuf, txbufsize);
        HAL_UART_Receive_IT(huart, &rxbyte, 1);
        return &uart;
}

int uart_write(uart_t *this, const void *data, int count)
{
        //HAL_UART_Transmit_IT(this->huart, (void*)data, count);
        //__disable_irq();
        int n = ringbuf_write(&this->txbuf, data, count);
        //__enable_irq();
        startTransmit(this);
        return n;
}

int uart_read(uart_t *this, void *data, int count)
{
        //__disable_irq();
        int ret = ringbuf_read(&this->rxbuf, data, count);
        //__enable_irq();
        return ret;
}
*/
#endif
