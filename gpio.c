#include "main.h"

#include "api/gpio.h"

static GPIO_TypeDef* const gpios[] = { GPIOA, GPIOB, GPIOC, GPIOD, GPIOE };
static uint32_t modes[5] = { GPIO_MODE_INPUT, GPIO_MODE_INPUT, GPIO_MODE_INPUT, GPIO_MODE_OUTPUT_PP, GPIO_MODE_OUTPUT_OD };

uint32_t gpio_get_pinmask(uint8_t port, unsigned pinmask)
{
    return gpios[port]->IDR & pinmask;
}

ret_t gpio_config(uint8_t port, unsigned pinmask, gpio_config_t config)
{
    GPIO_InitTypeDef init = { 0 };
    init.Pin = pinmask;
    init.Mode = modes[config];
    init.Speed = GPIO_SPEED_FREQ_HIGH;
    if (config == GPIO_INPUT_LOW) {
        init.Pull = GPIO_PULLDOWN;
    } else if (config == GPIO_INPUT_HIGH) {
        init.Pull = GPIO_PULLUP;
    }

    HAL_GPIO_Init(gpios[port], &init);
    return RET_OK;
}

ret_t gpio_toggle_pinmask(uint8_t port, unsigned pinmask)
{
    HAL_GPIO_TogglePin(gpios[port], pinmask);
    return RET_OK;
}

ret_t gpio_set_pinmask(uint8_t port, unsigned pinmask)
{
    gpios[port]->BSRR = pinmask;
    return RET_OK;
}

ret_t gpio_clear_pinmask(uint8_t port, unsigned pinmask)
{
    gpios[port]->BSRR = pinmask << 16;
    return RET_OK;
}

ret_t gpio_set_values(uint8_t port, unsigned pinmask, unsigned value)
{
    gpios[port]->BSRR = pinmask;
    gpios[port]->BSRR = (~pinmask) << 16;
    return RET_OK;
}
