#include <stdint.h>

#include "main.h"
#if 1
#define TIMER htim3

extern TIM_HandleTypeDef TIMER;

void delay_init(void)
{
    HAL_TIM_Base_Start(&TIMER);
    // HAL_TIM_OC_Start(&htim3, TIM_CHANNEL_1);
}

void delay_us(uint32_t us)
{
    if (us <= 5)
        return;

    us -= 5;
    uint32_t start = __HAL_TIM_GET_COUNTER(&TIMER); //__HAL_TIM_SET_COUNTER(&TIMER, 0);
    while ((__HAL_TIM_GET_COUNTER(&TIMER)) - start <= us)
        ;
}

uint32_t timer_us(void)
{
    return __HAL_TIM_GET_COUNTER(&TIMER);
}

void delay_ms(uint32_t ms)
{
    // HAL_Delay(ms);
    for (uint32_t i = 0; i < ms; i++) {
        delay_us(1000);
    }
}

#endif
