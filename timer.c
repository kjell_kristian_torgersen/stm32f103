#include "main.h"

static uint32_t timer_seconds = 0;
static uint32_t mstimer = 0;
static uint32_t unixtime = 0;

uint32_t timer_unixtime_get(void)
{
    return unixtime;
}

void timer_unixtime_set(uint32_t time)
{
    unixtime = time;
}

void timer_tick(void)
{
    mstimer++;
    if (mstimer >= 1000) {
        mstimer = 0;
        timer_seconds++;
        unixtime++;
    }
}

uint32_t timer_ms(void)
{
    return HAL_GetTick();
}

uint32_t timer_s(void)
{
    return timer_seconds;
}
